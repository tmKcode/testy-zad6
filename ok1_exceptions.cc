#include <cassert>
#include "rover.h"

int main() {
    auto rover = RoverBuilder().build();
    rover.land({0, 0}, Direction::EAST);
	try {
		rover.land({0, 0}, Direction::EAST);
		assert(false);
	}
	catch (...) {
	}
}
